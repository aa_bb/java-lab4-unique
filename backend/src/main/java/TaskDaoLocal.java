import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class TaskDaoLocal implements TaskDao {

    ArrayList<Task> allTasks;

    public TaskDaoLocal() {
        try {
            ObjectInputStream in;
            in = new ObjectInputStream(new FileInputStream("allTasks"));
            allTasks = (ArrayList<Task>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Task> getAll() {
        return allTasks;
    }

    @Override
    public void addTask(Task newTask) {
        allTasks.add(newTask);
    }

    @Override
    public void changeTask(Task changingTask, String name, String description, boolean isFinished, Optional<LocalDate> deadline, ArrayList<String> tags, ArrayList<SubTask> subTasks) {
        changingTask.setName(name);
        changingTask.setDescription(description);
        changingTask.setFinished(isFinished);
        changingTask.setDeadline(deadline);
        changingTask.setTags(tags);
        changingTask.setSubTasks(subTasks);
    }

    @Override
    public void deleteTask(Task task) {
        allTasks.remove(task);
    }

    @Override
    public void saveStateTasks() {
        try {
            FileOutputStream fos = new FileOutputStream("allTasks");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(allTasks);
            oos.close();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public ArrayList<Task> getUnfinishedTasks() {
        return (ArrayList<Task>) allTasks.stream()
                .filter(task -> !task.isFinished()).collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getUnfinishedOverdueAndDeadlineInNearWeekTasks() {
        return (ArrayList<Task>) allTasks.stream()
                .filter(task -> !task.isFinished() &&
                        task.getOptionalDeadline().isPresent() &&
                        (task.getOptionalDeadline().get().isAfter(LocalDate.now().minusDays(1)) &&
                                task.getOptionalDeadline().get().isBefore(LocalDate.now().plusDays(8))))
                .sorted(comparatorNoNullDateAndName())
                .collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getUnfinishedTasksByTag(String tag) {
        return (ArrayList<Task>) allTasks.stream()
                .filter(task -> !task.isFinished() && task.containsTag(tag))
                .sorted((a, b) -> a.getName().compareToIgnoreCase(b.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getFinishedTasks() {
        return (ArrayList<Task>) allTasks.stream()
                .filter(SubTask::isFinished)
                .sorted((a, b) -> a.getName().compareToIgnoreCase(b.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getOnNearestMonthWithTagAndSubStr(String tag, String subStr) {
        return (ArrayList<Task>) allTasks.stream()
                .filter(task -> task.getOptionalDeadline().isPresent()
                        && task.getOptionalDeadline().get().isAfter(LocalDate.now().minusDays(1))
                        && task.getOptionalDeadline().get().isBefore(LocalDate.now().plusMonths(1).plusDays(1))
                        && task.containsTag(tag)
                        && task.containsSubStrInDescription(subStr))
                .collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getUnfinishedMoreHalfSubTasksFinished() {
        return (ArrayList<Task>) allTasks
                .stream()
                .filter(task -> !task.isFinished() &&
                        task.getSubTasks().size() != 0 &&
                        task.getSubTasks().size() <=
                                (int) task.getSubTasks().stream().filter(SubTask::isFinished).count() * 2)
                .collect(Collectors.toList());
    }

    public ArrayList<String> getThreePopularTags() {
        ArrayList<String> allTags = new ArrayList<>();
        allTasks.forEach(t -> allTags.addAll(t.getTags()));

        LinkedHashMap<String, Integer> countTags = new LinkedHashMap<String, Integer>();

        //получили словарь тэгов с количеством повторений для каждого тэга

        allTags.forEach(tag -> countTags.put(tag.toLowerCase(), countTags.getOrDefault(tag.toLowerCase(), 0) + 1));

        ArrayList<String> threePopularTags = new ArrayList<>();

        countTags.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue((k1, k2) -> k2 - k1))
                .limit(3)
                .forEach(x -> threePopularTags.add(x.getKey()));

        return threePopularTags;
    }

    @Override
    public ArrayList<Task> getOverdueWithOneMoreTagFromThreePopular() {
        ArrayList<String> threePopularTags = getThreePopularTags();
        return (ArrayList<Task>) allTasks
                .stream()
                .filter(task ->
                        task.getOptionalDeadline().isPresent()
                                && task.getOptionalDeadline().get().isBefore(LocalDate.now())
                                &&
                                task
                                        .getTags()
                                        .stream()
                                        .anyMatch(taskTag -> threePopularTags.stream()
                                                .anyMatch(pT -> pT.equalsIgnoreCase(taskTag))))
                .collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getThreeWithNearestDeadlineWithTag(String tag) {
        ArrayList<Task> tasksWithTagAndNearestDeadline = (ArrayList<Task>) allTasks.stream()
                .filter(task -> task.containsTag(tag)
                        && task.getOptionalDeadline().isPresent()
                        && (task.getOptionalDeadline().get().isAfter(LocalDate.now())
                        || task.getOptionalDeadline().get().isEqual(LocalDate.now())
                ))
                .sorted(comparatorNullableDate(1)).limit(3)
                .collect(Collectors.toList());
        tasksWithTagAndNearestDeadline.sort(Comparator.comparing(Task::getDeadline));
        return (ArrayList<Task>) tasksWithTagAndNearestDeadline.stream().limit(3).collect(Collectors.toList());
    }

    @Override
    public ArrayList<Task> getFourWithFarDeadlineWithoutTag() {
        ArrayList<Task> tasksWithoutTags = (ArrayList<Task>) allTasks.stream()
                .filter(task -> task.getTags().isEmpty() && task.getOptionalDeadline().isPresent())
                .collect(Collectors.toList());
        Collections.sort(tasksWithoutTags, (t1, t2) -> t2.getDeadline().compareTo(t1.getDeadline()));
        return (ArrayList<Task>) tasksWithoutTags.stream().limit(4).collect(Collectors.toList());
    }

    Comparator<Task> comparatorNoNullDateAndName() {
        return (task1, task2) -> {
            long d1 = (task1).getOptionalDeadline().get().getDayOfYear();
            long d2 = (task2).getOptionalDeadline().get().getDayOfYear();
            Date date1 = new Date(d1);
            Date date2 = new Date(d2);
            int sComp = date1.compareTo(date2);
            if (sComp != 0)
                return sComp;
            String name1 = (task1).getName();
            String name2 = (task2).getName();
            return name1.compareTo(name2);
        };
    }

    Comparator<Task> comparatorNullableDate(int order) {
        return (task1, task2) -> {
            if (order == 0)
                throw new IllegalArgumentException("Неверный порядок сортировки");
            // не ФП - два null в проверке + нужно сохранить return
            if (!task1.getOptionalDeadline().isPresent() && !task2.getOptionalDeadline().isPresent())
                return 0;
            if (!task1.getOptionalDeadline().isPresent())
                return order;
            if (!task2.getOptionalDeadline().isPresent())
                return -order;
            return order < 0 ?
                    task2.getOptionalDeadline().get().compareTo(task1.getOptionalDeadline().get()) :
                    task1.getOptionalDeadline().get().compareTo(task2.getOptionalDeadline().get());
        };
    }
}